from rest_framework import serializers

from mgr.models import DLAccount


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = DLAccount
        fields = '__all__'