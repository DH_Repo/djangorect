import json

from django.core import serializers
from django.forms import model_to_dict
from django.shortcuts import render

# Create your views here.
from mgr.models import DLAccount


def account_list_view(request):
    context = {}

    return render(request, 'service/account_list.html', context=context)