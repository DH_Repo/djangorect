from django.urls import path
from service import views
from service._requests import _ajax

app_name = 'service'
urlpatterns = [
    path('member/list', views.account_list_view, name='account_list'),
    path('ajax/member/list', _ajax.account_list, name='ajax_account_list'),
]