from django.db import models

# Create your models here.
class DLBoard(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False, default='')
    content = models.TextField(null=True, blank=True)
    order = models.IntegerField(null=True, blank=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    modifiedDate = models.DateTimeField(auto_now=True)
    mainImg = models.ImageField(null=True, blank=True)
    account = models.ForeignKey('mgr.DLAccount', blank=True, null=True, related_name='account_board', default='0',
                                on_delete=models.SET_NULL)
    class Meta:
        db_table = 'service_board'