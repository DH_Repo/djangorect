import json

from django.core import serializers
from django.forms import model_to_dict
from django.http import JsonResponse, HttpResponse

from mgr.models import DLAccount
from service.serializers import AccountSerializer


def account_list(requset):
    # results = [ob.as_json() for ob in obj]
    # j = json.dumps(results)
    # 1. 모델의 object (manager) 통해서 query를 입력하면 해당 컬럼이름과 동기화하는 방식
    # obj1 = DLAccount.objects.raw('SELECT name FROM mgr_account')
    # 2. 모델의 object의 all 이나 filter를 써서 쿼리를 알아서 만들어 주는 방식
    # DLAccount.objects.filter(name='김철수')
    obj = DLAccount.objects.all()
    # python dictionary를 string으로 변환
    a = {'bb': 'cc'}
    b = json.dumps(a)
    # django restframework를 사용해서 (api서버시 사용) model를 serialize 해준다.
    accountList = AccountSerializer(obj, many=True)
    result = {'state': 'success', 'result': accountList.data}
    return JsonResponse(result)