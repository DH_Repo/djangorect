from django import forms

from mgr.models import DLAccount


class SignUpForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, label='패스워드 입력')
    class Meta:
        model = DLAccount
        fields = ['name', 'password', 'address', 'age', 'nickName', 'profileImg']
        labels ={
            'name':'이름'
        }
