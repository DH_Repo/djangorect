from django.db import models

# Create your models here.
class DLAccount(models.Model):
    name = models.CharField(max_length=20, null=False, blank=False, default='')
    address = models.TextField(null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    nickName = models.CharField(max_length=20, null=True, blank=True)
    profileImg = models.ImageField(null=True, blank=True)
    user = models.OneToOneField('auth.User', blank=False, null=False, related_name='user_account', default='0',
                                on_delete=models.CASCADE)
    class Meta:
        db_table = 'mgr_account'
