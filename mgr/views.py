from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import FormView

from mgr.forms import SignUpForm


class SignUpView(FormView):
    template_name = 'mgr/sign_up.html'
    form_class = SignUpForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        myform = form.save(commit=False)
        # 회원 가입이기 때문에 django 기본 유저를 새로 생성하는 부분.
        user = User()
        user.username = myform.name
        user.password = make_password(form.cleaned_data['password'])
        user.save()
        # 저장한 유저를 account 모델에 저장.
        myform.user = user
        myform.save()
        form.save_m2m()
        return super().form_valid(form)

    def get(self, request, *args, **kwargs):
        context = super().get_context_data()
        context['info'] = "회원가입페이지 입니다."
        return self.render_to_response(context)





# template 를 호출 할때, class view 및 function view 가 있음.
# class view 는 django의 여러가지 (ex. FormView) 를 상속받는데, 이 다양한 views 들은 특정 목적에서 조금 편하게 할 수 있도록
# 상속을 거쳐서 구조화 되어 잇음.

# function view 는 request에 대한 모든 작업을 다 해줘야하는 (get, post 별로 다 각각 직접처리해야함) views(장고의 템플릿 return 부)