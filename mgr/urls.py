from django.urls import path

from mgr import views

app_name = 'mgr'
urlpatterns = [
    path('signup', views.SignUpView.as_view(), name='sign_up'),
]